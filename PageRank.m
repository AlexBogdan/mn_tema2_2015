# Functia genereaza R1 R2 R3 folosind functiile de la
# taskurile anterioare , dupa foloseste R2 pentru a genera
# taskul 4. Toate acestea sunt scrise in fisierul de output

function [R1 R2 R3] = PageRank(nume, d, eps)

R1 = Iterative (nume , d , eps);
R2 = Algebraic (nume , d);
R3 = Power (nume , d , eps);

input = importdata(nume," ");%Citiri
graf_dim = input(1,1); 
val1 = input(1+graf_dim + 1,1);
val2 = input(1+graf_dim + 2,1);

PR1(:,1) = 1:graf_dim;
PR1(:,2) = PR1(:,1); %Pregatire PR1
PR1(:,3) = R2;

for i=1:graf_dim-1     %Forul de sortare
	rand = i;
	maximul = PR1(i,3);
	for j=i+1:graf_dim
		if maximul < PR1(j,3) && abs(maximul - PR1(j,3)) > 10^-10
			rand = j;
			maximul = PR1(j,3);
		end
	end
	aux = PR1(i,3);
	PR1(i,3) = PR1(rand,3);
	PR1(rand,3) = aux;
	aux = PR1(i,2);
	PR1(i,2) = PR1(rand,2);
	PR1(rand,2) = aux;
end

for i=1:graf_dim    % Aplicam functia taskului 4
	PR1 (i,3) = Apartenenta (PR1(i,3) , val1 , val2);
end

nume = [nume ".out"];
save ("-ascii" , nume , "graf_dim" , "R1" , "R2" , "R3" , "PR1");

endfunction