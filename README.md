=-=--=-=-=-=-=-=- Andrei Bogdan Alexandru -=-=-=-=-=-=-
-=-=-=-=-=-=-=-=-=-=-=-= 314 CA -=-=-=--=-=-=-=-=-=-=-=
-=-=-=-=-=-=----==--=-= Tema 2 MN -=-=-=-=-=-=-=-

Pentur fiecare task din cerinta am facut citirea si am selectat 
datele necesare rezolvarii taskului respectiv.

	Task 1

Dupa citire , slectam doar partea din graf care ne intereseaza,
anume vecinii fiecarui nod si formam matricea de adiacenta.
Matricea M se va forma dupa regula 1/L(b) din enunt.

Recurenta va fi efectuata pana cand toti termenii vor fi sub
valoarea epsilon.

	Task 2

Prima parte este aceeasi cu cea de la task1 
Gram Schmidt.m este luat din laborator , iar functia invers va
genera inversa unei matrice inferior triunghiulara (scrisa de mine
la Tema1)

	Task 3

Identic cu taskul 1 , doar formula de recurenta difera.

	Task 4 

Formeaza R1 , R2 si R3 folosind functiile scrise anterior.
Apartentea.m calculeaza a si b astfel incat functia sa fie continua
si returneaza valoarea pentru inputul x , forman PR1

Toate rezultatele sunt scrise in fisierul .out prin functia save.
