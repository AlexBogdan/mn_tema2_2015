# din a*val1 + b = 0 si a*val2 + b = 1 =>
# => a*(val2 - val1) = 1 =>
# => a = 1/(val2 - val1) si b = - a*val1

function y = Apartenenta(x, val1, val2)

a = 1/(val2 - val1);
b = -a*val1;
y=a*x + b;

if x < val1   %ramurile functiei
	y = 0;
else
	if x > val2
		y = 1;
	end
end
endfunction