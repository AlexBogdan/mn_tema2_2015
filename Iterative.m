# In aceasta functie am extras datele din fisierul de intrare ,
# am pastrat in A_raw doar vecinii fiecarui nod , am generat
# matricea de adiacenta , am generat M si am implementat
# formula din enunt. 
# graf_dim = N din enunt , in toate fisierele

function R = Iterative(nume, d, eps)

input = importdata(nume," ");

graf_dim = input(1,1); 
A_raw=input(2:(graf_dim+1),:);
A_raw = A_raw(:,3:graf_dim);      %Luam doar partea care ne trebuie
A=zeros(graf_dim);

for i=1:graf_dim    %Am generat matricea de adiacenta
	A(i,A_raw(i , A_raw(i,:) ~= 0)) = 1;
	A(i,i) = 0;
end

for i=1:graf_dim    %Am generat matricea M
	for j=1:graf_dim
		if A(j,i) == 1
			M(i,j) = 1/sum(A(j,:));
		else
			M(i,j) = 0;
		end
	end
end

unul_coloana(1:graf_dim,1) = 1;   %Vectorul coloana de 1
R_anterior(1:graf_dim,1) = 1/graf_dim;   %Primul pas din recurenta
R = d*M*R_anterior + ((1-d)/graf_dim)*unul_coloana;

while sum(abs(R - R_anterior < eps)) ~= graf_dim
	R_anterior = R;
	R = d*M*R_anterior + ((1-d)/graf_dim)*unul_coloana;
end     %Formula din enunt

endfunction