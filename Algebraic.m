# Am folosit Gram_Schmidt pentru a obtine
# o forma QR , folosind codul din laborator.
# Functia "invers" inverseaza o matrice triunghiulara

function R = Algebraic(nume, d)

input = importdata(nume," ");

graf_dim = input(1,1); 
A_raw=input(2:(graf_dim+1),:);
A_raw = A_raw(:,3:graf_dim); %Luam doar partea care ne trebuie
A=zeros(graf_dim);

for i=1:graf_dim
	A(i,A_raw(i , A_raw(i,:) ~= 0)) = 1;
	A(i,i) = 0;
end   %Am generat matricea de adiacenta

for i=1:graf_dim
	for j=1:graf_dim
		if A(j,i) == 1
			M(i,j) = 1/sum(A(j,:));
		else
			M(i,j) = 0;
		end
	end
end %Am generat matricea M

unul_coloana(1:graf_dim,1) = 1; %Vectorul coloana de 1
I = eye(graf_dim);
A = I - d*M;
[Q r] = Gram_Schmidt (A);   %QR-ul din laborator
R = invers(r')'*Q' * ((1-d)/graf_dim)*unul_coloana;

endfunction