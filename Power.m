# In mare parte aceeasi functie ca cea de la 
# taskul 1 , schimbam doar formula de la 
# while-ul final

function R = Power(nume, d, eps)

input = importdata(nume," ");

graf_dim = input(1,1); 
A_raw=input(2:(graf_dim+1),:);
A_raw = A_raw(:,3:graf_dim);
A=zeros(graf_dim);

for i=1:graf_dim
	A(i,A_raw(i , A_raw(i,:) ~= 0)) = 1;
	A(i,i) = 0;
end   %Am generat matricea de adiacenta

for i=1:graf_dim
	for j=1:graf_dim
		if A(j,i) == 1
			M(i,j) = 1/sum(A(j,:));
		else
			M(i,j) = 0;
		end
	end
end %Am generat matricea M

unul_matrice = ones(graf_dim);
P = d*M + ((1-d)/graf_dim)*unul_matrice;
R_anterior(1:graf_dim,1) = 1/graf_dim; %Primul pas din recurenta
z = P * R_anterior;
R = z/norm(z,1);

while sum(abs(R - R_anterior < eps)) ~= graf_dim
	R_anterior = R;
	z = P * R_anterior;
	R = z/norm(z,1); //Astfel aflam vectorul propriu cerut
end

endfunction
